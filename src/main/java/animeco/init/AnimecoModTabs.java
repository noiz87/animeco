
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package animeco.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.BuildCreativeModeTabContentsEvent;

import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.network.chat.Component;
import net.minecraft.core.registries.Registries;

import animeco.AnimecoMod;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class AnimecoModTabs {
	public static final DeferredRegister<CreativeModeTab> REGISTRY = DeferredRegister.create(Registries.CREATIVE_MODE_TAB, AnimecoMod.MODID);
	public static final RegistryObject<CreativeModeTab> ANIME_TAB = REGISTRY.register("anime_tab",
			() -> CreativeModeTab.builder().title(Component.translatable("item_group.animeco.anime_tab")).icon(() -> new ItemStack(AnimecoModItems.TAB_ICON.get())).displayItems((parameters, tabData) -> {
				tabData.accept(AnimecoModItems.HERO_SWORD.get());
				tabData.accept(AnimecoModItems.HERO_SHIELD.get());
				tabData.accept(AnimecoModItems.HERO_SPEAR.get());
				tabData.accept(AnimecoModBlocks.LUIGI.get().asItem());
				tabData.accept(AnimecoModItems.HERO_BOW.get());
				tabData.accept(AnimecoModBlocks.MARIO.get().asItem());
			})

					.build());

	@SubscribeEvent
	public static void buildTabContentsVanilla(BuildCreativeModeTabContentsEvent tabData) {
		if (tabData.getTabKey() == CreativeModeTabs.SPAWN_EGGS) {
			tabData.accept(AnimecoModItems.NAUFUMI_SPAWN_EGG.get());
		}
	}
}
