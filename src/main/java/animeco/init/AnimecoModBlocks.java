
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package animeco.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.world.level.block.Block;

import animeco.block.MarioBlock;
import animeco.block.LuigiBlock;

import animeco.AnimecoMod;

public class AnimecoModBlocks {
	public static final DeferredRegister<Block> REGISTRY = DeferredRegister.create(ForgeRegistries.BLOCKS, AnimecoMod.MODID);
	public static final RegistryObject<Block> LUIGI = REGISTRY.register("luigi", () -> new LuigiBlock());
	public static final RegistryObject<Block> MARIO = REGISTRY.register("mario", () -> new MarioBlock());
	// Start of user code block custom blocks
	// End of user code block custom blocks
}
