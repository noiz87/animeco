
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package animeco.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.common.ForgeSpawnEggItem;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.BlockItem;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.client.renderer.item.ItemProperties;

import animeco.item.TabIconItem;
import animeco.item.HeroSwordItem;
import animeco.item.HeroSpearItem;
import animeco.item.HeroShieldItem;
import animeco.item.HeroBowItem;

import animeco.AnimecoMod;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class AnimecoModItems {
	public static final DeferredRegister<Item> REGISTRY = DeferredRegister.create(ForgeRegistries.ITEMS, AnimecoMod.MODID);
	public static final RegistryObject<Item> TAB_ICON = REGISTRY.register("tab_icon", () -> new TabIconItem());
	public static final RegistryObject<Item> HERO_SWORD = REGISTRY.register("hero_sword", () -> new HeroSwordItem());
	public static final RegistryObject<Item> HERO_SHIELD = REGISTRY.register("hero_shield", () -> new HeroShieldItem());
	public static final RegistryObject<Item> NAUFUMI_SPAWN_EGG = REGISTRY.register("naufumi_spawn_egg", () -> new ForgeSpawnEggItem(AnimecoModEntities.NAUFUMI, -1, -1, new Item.Properties()));
	public static final RegistryObject<Item> HERO_SPEAR = REGISTRY.register("hero_spear", () -> new HeroSpearItem());
	public static final RegistryObject<Item> LUIGI = block(AnimecoModBlocks.LUIGI);
	public static final RegistryObject<Item> HERO_BOW = REGISTRY.register("hero_bow", () -> new HeroBowItem());
	public static final RegistryObject<Item> MARIO = block(AnimecoModBlocks.MARIO);

	// Start of user code block custom items
	// End of user code block custom items
	private static RegistryObject<Item> block(RegistryObject<Block> block) {
		return REGISTRY.register(block.getId().getPath(), () -> new BlockItem(block.get(), new Item.Properties()));
	}

	@SubscribeEvent
	public static void clientLoad(FMLClientSetupEvent event) {
		event.enqueueWork(() -> {
			ItemProperties.register(HERO_SHIELD.get(), new ResourceLocation("blocking"), ItemProperties.getProperty(Items.SHIELD, new ResourceLocation("blocking")));
		});
	}
}
