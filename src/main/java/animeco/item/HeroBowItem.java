
package animeco.item;

import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.BowItem;

public class HeroBowItem extends BowItem {
	public HeroBowItem() {
		super(new Item.Properties().stacksTo(1).rarity(Rarity.EPIC));
	}

	@Override
	public int getEnchantmentValue() {
		return 20;
	}
}
